﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz19_Facade
{
    /// <summary>
    /// Клас реализующий функциона лсоздания мотиватора
    /// </summary>
    public class Motivator
    {
        private Bitmap _buf;
        private Graphics _graphics;
        int margin = 20;
        float koefText = 0.2f;

        public Motivator(int height, int width)
        {
            _buf = new Bitmap(height, width);  // с размерами
            _graphics = Graphics.FromImage(_buf);
            _graphics.Clear(Color.FromArgb(74, 20, 76));
        }

        public string getNewImageName(string imageName)
        {
            return System.IO.Path.GetFileNameWithoutExtension(imageName)
                        + "_new" + System.IO.Path.GetExtension(imageName);
        }

        public void SetFontColor(Color color)
        {
            _graphics.Clear(color);
        }

        public void SetImage(Bitmap bmp)
        {
            _graphics.DrawImage(bmp, margin, margin, _buf.Width - margin * 2, _buf.Height - _buf.Height * koefText - 2 * margin);
            _graphics.DrawRectangle(new Pen(Color.White, 5), margin - 2, margin - 2, _buf.Width - margin * 2 + 2 * 2, _buf.Height - _buf.Height * koefText - 2 * margin + 2 * 2);
        }
        public void SetText(string text)
        {
            RectangleF rect = new RectangleF(margin, _buf.Height - _buf.Height * koefText, _buf.Width-2* margin, _buf.Height * koefText);
            StringFormat format = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            _graphics.DrawString(text, new Font("Tahoma", 30), new SolidBrush(Color.FromArgb(233, 252, 255)), rect, format);
        }

        public void SaveToFile(string FileName)
        {
            _buf.Save(FileName);
        }


    }
}
