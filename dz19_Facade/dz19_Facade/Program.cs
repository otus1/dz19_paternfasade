﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace dz19_Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "";
            
            fileName = MotivatorFacade.CreateMotivator("img1.jpg", "Нет полного счаться \n есть только счастливые моменты");
            Console.WriteLine($"Newimage file name: {fileName}");

            fileName = MotivatorFacade.CreateMotivator("img2.jpg", "Баланс... бывает не только \n финансовым");
            Console.WriteLine($"Newimage file name: {fileName}");

            fileName = MotivatorFacade.CreateMotivator("img3.jpg", "Красота познается в сравнении");
            Console.WriteLine($"Newimage file name: {fileName}");

            Console.ReadKey();

        }
    }
}
