﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz19_Facade
{
    public class MotivatorFacade
    {
        static public string CreateMotivator(string image, string message)
        {            
            Bitmap bmp = new Bitmap(image);

            Motivator motivator = new Motivator(800, 800);
            string newFileName = motivator.getNewImageName(image);
            motivator.SetFontColor(Color.FromArgb(75, 135, 216));
            motivator.SetImage(bmp);
            motivator.SetText(message);
            motivator.SaveToFile(newFileName);
            return newFileName;
        }
    }
}
